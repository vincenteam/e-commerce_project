<%-- 
    Document   : checkout
    Created on : Dec 1, 2015, 10:02:14 PM
    Author     : Jean
--%>

<div class="container">
    <div class="col-md-6">
        <h2>Checkout</h2>
        <form action="purchase" method="post" class="form-horizontal">
            <p><fmt:message key='CheckoutText'/>:</p>
            <div class="form-group">
                <label class="control-label col-xs-3" for="firstName"><fmt:message key='First name'/>:</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" id="firstName" placeholder="<fmt:message key='First name'/>" name="firstName" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3" for="lastName"><fmt:message key='Last name'/>:</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" id="lastName" placeholder="<fmt:message key='Last name'/>" name="familyName" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3" for="inputEmail"><fmt:message key='Email'/>:</label>
                <div class="col-xs-9">
                    <input type="email" class="form-control" id="inputEmail" placeholder="<fmt:message key='Email'/>" name="email" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3" for="phoneNumber"><fmt:message key='Phone'/>:</label>
                <div class="col-xs-9">
                    <input type="tel" class="form-control" id="phoneNumber" placeholder="<fmt:message key='Phone'/>" name="phone" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3" for="postalAddress"><fmt:message key='Address'/>:</label>
                <div class="col-xs-9">
                    <textarea rows="3" class="form-control" id="postalAddress" placeholder="<fmt:message key='Address'/>" name="address" required></textarea>
                </div>
            </div>  
            <div class="form-group">
                <label class="control-label col-xs-3" for="cc_number"><fmt:message key='Credit card'/>:</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" id="cc_number" placeholder="<fmt:message key='Credit card'/>" name="creditcard" required>
                </div>
            </div>   
            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <input type="submit" class="btn btn-primary" value="<fmt:message key='Submit purchase'/>">

                </div>
            </div>                                            
        </form>
    </div>

    <div class="col-md-6">
        <h2><fmt:message key='Summary'/></h2>
        <ul class="list">
            <li><fmt:message key='SummaryText1'/></li>
            <li>
                <fmt:message key='DeliveryMessage'/></li>
        </ul>

        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td><fmt:message key='Subtotal'/>:</td>
                    <td class="checkoutPriceColumn">
                        &euro; ${cart.subtotal}</td>
                </tr>
                <tr>
                    <td><fmt:message key='DeliverySurcharge'/>:</td>
                    <td class="checkoutPriceColumn">
                        &euro; ${initParam.deliverySurcharge}</td>
                </tr>
                <tr>
                    <td class="total"><fmt:message key='Total'/>:</td>
                    <td class="total checkoutPriceColumn">
                        &euro; ${cart.total}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>