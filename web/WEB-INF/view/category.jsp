
<%-- 
    Document   : category
    Created on : Dec 1, 2015, 10:01:11 PM
    Author     : Jean
--%>
<%--<sql:query var="categories" dataSource="jdbc/mydb">
    SELECT * FROM categories;
</sql:query>

<sql:query var="categoryProducts" dataSource="jdbc/mydb">
    SELECT * FROM produits WHERE Categories_idCategories= ?
    <sql:param value="${pageContext.request.queryString}"/>
</sql:query>--%>

<div class="container">
    
        <div class="col-md-3">
            <p class="lead"><fmt:message key='Categories'/></p>
            <div class="list-group">
                <c:forEach var="category" items="${categories}">
                    <c:choose>
                        <c:when test="${category.nom == selectedCategory.nom}">
                            <span class = "list-group-item active"><fmt:message key='${category.nom}'/></span>  
                        </c:when>
                        <c:otherwise>
                            <a href="category?${category.idCategories}" class = "list-group-item"><fmt:message key='${category.nom}'/></a>  
                        </c:otherwise>
                    </c:choose>

                </c:forEach>               
            </div>
        </div>

        <div class="col-md-9">
            <div class="row">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="width:25%;"><fmt:message key='Products'/></th>
                            <th style="width:25%;"><fmt:message key='Description'/></th>
                            <th style="width:25%;"><fmt:message key='Price'/></th>
                            <th style="width:25%;"><fmt:message key='Select'/></th>
                        </tr>
                    </thead>
                    <tbody>                      
                    <c:forEach var="product" items="${categoryProducts}" varStatus="iter">

                        <tr>
                            <td>
                                <img src="${initParam.productImagePath}${product.nom}.jpg"
                                     class="img-responsive"
                                     alt="${product.nom}">
                            </td>
                            <td>
                                <strong>${product.nom}</strong>
                                <br>
                                <span>${product.description}</span>
                            </td>
                            <td>
                                &euro;${product.prix}
                            </td>
                            <td>
                                <form action="addToCart" method="post">
                                    <input type="hidden"
                                           name="productId"
                                           value="${product.idProduits}">
                                    <input type="submit"
                                           name="submit"
                                           value="<fmt:message key='Add to cart'/>">
                                </form>
                            </td>
                        </tr>

                    </c:forEach>
                    </tbody>
                </table>              
            </div>
        </div>
    
</div>