CREATE DATABASE  IF NOT EXISTS `mydb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `mydb`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	5.7.9-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `idCategories` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  PRIMARY KEY (`idCategories`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Les différentes catégories de produits';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Dresses'),(2,'Sweaters'),(3,'Jeans');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `idClients` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  `prenom` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `adresse` varchar(45) NOT NULL,
  `telephone` varchar(45) NOT NULL,
  `cc_number` varchar(45) NOT NULL,
  PRIMARY KEY (`idClients`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Détail des clients';
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `commandes`
--

DROP TABLE IF EXISTS `commandes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commandes` (
  `idCommandes` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `montant` decimal(6,2) NOT NULL,
  `dateCreation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `numeroConfirmation` int(10) unsigned NOT NULL,
  `Clients_idClients` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idCommandes`),
  KEY `fk_Commandes_Clients1_idx` (`Clients_idClients`),
  CONSTRAINT `fk_Commandes_Clients1` FOREIGN KEY (`Clients_idClients`) REFERENCES `clients` (`idClients`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Détail des commandes';
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `produits`
--

DROP TABLE IF EXISTS `produits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produits` (
  `idProduits` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  `prix` decimal(5,2) NOT NULL,
  `description` text,
  `derniereMaj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `disponibilite` int(10) unsigned NOT NULL DEFAULT '0',
  `Categories_idCategories` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idProduits`),
  KEY `fk_Produits_Categories1_idx` (`Categories_idCategories`),
  CONSTRAINT `fk_Produits_Categories1` FOREIGN KEY (`Categories_idCategories`) REFERENCES `categories` (`idCategories`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='Détail des produits';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produits`
--

LOCK TABLES `produits` WRITE;
/*!40000 ALTER TABLE `produits` DISABLE KEYS */;
INSERT INTO `produits` VALUES (1,'Sleeveless Boatneck',745.00,'Sleeveless boat neck structured gown with side cut outs and high slit at skirt.','2015-12-01 17:40:58',1,1),(2,'Sleeveless V Neck',338.00,'Sleeveless v neck beaded gown with mesh insets at hem.','2015-12-01 17:40:58',1,1),(3,'Full Skirt Dress',140.00,'Lace bodice with illusion neckline and uneven full chiffon skirt with self-fabric sash.','2015-12-01 17:40:58',1,1),(4,'Long Sleeve Cowl Neck',134.00,'Long sleeve fit and flare sweater dress with detachable scarf.','2015-12-01 17:40:58',1,1),(5,'Juniors\' Penguin',29.00,'Long sleeve penguin with jingle bells and button eyes ugly christmas sweater, great for an ugly christmas party.','2015-12-01 01:41:31',1,2),(6,'Juniors\' Panda Bear',29.00,'Panda bear with scarf and mittens pull over tunic ugly christmas sweater, great for an ugly christmas party.','2015-12-01 01:43:31',1,2),(7,'Women\'s Sweater Knit',89.50,'Speckle yarn sweater with woven shirting at hemline','2015-12-01 01:45:31',1,2),(8,'Women\'s Shadow Stripe',79.50,'DKNY Jeans woman\'s shadow stripe boyfriend pullover','2015-12-01 01:47:31',1,2),(9,'Women\'s A Pocket Jean',81.97,'A medium blue wash with excellent stretch and recovery. Whiskers and hand sanding down the front thighs add an authentic feel. Contrast thread and silver hardware for a vintage inspired look.','2015-12-01 01:57:31',1,3),(10,'Women\'s Slouchy Slim',137.45,'Relaxed-fit jean with a medium rise and slightly tapered leg. Cut to an ankle length','2015-12-01 01:58:31',1,3),(11,'Women\'s High Rise Bell',199.00,'Made from our most luxuriously soft transcend fabric. Using the latest performance fiber technology, this denim features an innovative formula that combines chic with comfort and won\'t stretch out no matter what. It has true denim appeal and moves with you all day.','2015-12-01 02:58:31',1,3),(12,'Women\'s Verdugo Ankle',209.00,'Made from our most luxuriously soft transcend fabric. Using the latest performance fiber technology, this denim features an innovative formula that combines chic with comfort and won\'t stretch out no matter what. It has true denim appeal and moves with you all day.','2015-12-01 03:58:31',1,3);
/*!40000 ALTER TABLE `produits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produits_commandes_retouches`
--

DROP TABLE IF EXISTS `produits_commandes_retouches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produits_commandes_retouches` (
  `Produits_idProduits` int(10) unsigned NOT NULL,
  `Commandes_idCommandes` int(10) unsigned NOT NULL,
  `Retouches_idRetouches` int(10) unsigned NOT NULL,
  `quantite` smallint(5) unsigned NOT NULL DEFAULT '1',
  `couleur` varchar(45) NOT NULL,
  `taille` varchar(45) NOT NULL,
  PRIMARY KEY (`Produits_idProduits`,`Commandes_idCommandes`),
  KEY `fk_Produits_has_Commandes_Commandes1_idx` (`Commandes_idCommandes`),
  KEY `fk_Produits_has_Commandes_Produits1_idx` (`Produits_idProduits`),
  KEY `fk_Produits_has_Commandes_Retouches1_idx` (`Retouches_idRetouches`),
  CONSTRAINT `fk_Produits_has_Commandes_Commandes1` FOREIGN KEY (`Commandes_idCommandes`) REFERENCES `commandes` (`idCommandes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Produits_has_Commandes_Produits1` FOREIGN KEY (`Produits_idProduits`) REFERENCES `produits` (`idProduits`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Produits_has_Commandes_Retouches1` FOREIGN KEY (`Retouches_idRetouches`) REFERENCES `retouches` (`idRetouches`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `profils`
--

DROP TABLE IF EXISTS `profils`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profils` (
  `idProfils` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sexe` varchar(45) NOT NULL,
  `couleur` varchar(45) NOT NULL,
  `taille` varchar(45) NOT NULL,
  `Clients_idClients` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idProfils`),
  KEY `fk_Profils_Clients1_idx` (`Clients_idClients`),
  CONSTRAINT `fk_Profils_Clients1` FOREIGN KEY (`Clients_idClients`) REFERENCES `clients` (`idClients`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Détail des profils';
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `retouches`
--

DROP TABLE IF EXISTS `retouches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retouches` (
  `idRetouches` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  `description` text,
  PRIMARY KEY (`idRetouches`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Détail des retouches';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `retouches`
--



-- Dump completed on 2015-12-08 22:13:39
