/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Profils;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Jean
 */
@Stateless
public class ProfilsFacade extends AbstractFacade<Profils> {

    @PersistenceContext(unitName = "WeDressYouPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProfilsFacade() {
        super(Profils.class);
    }
    
}
