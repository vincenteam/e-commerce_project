/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.ProduitsCommandesRetouches;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Jean
 */
@Stateless
public class ProduitsCommandesRetouchesFacade extends AbstractFacade<ProduitsCommandesRetouches> {

    @PersistenceContext(unitName = "WeDressYouPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProduitsCommandesRetouchesFacade() {
        super(ProduitsCommandesRetouches.class);
    }
    
    public List<ProduitsCommandesRetouches> findByOrderId(Object id) {
        return em.createNamedQuery("ProduitsCommandesRetouches.findByCommandesidCommandes").setParameter("commandesidCommandes", id).getResultList();
    }
}
