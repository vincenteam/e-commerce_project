/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Commandes;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Jean
 */
@Stateless
public class CommandesFacade extends AbstractFacade<Commandes> {

    @PersistenceContext(unitName = "WeDressYouPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CommandesFacade() {
        super(Commandes.class);
    }
    
}
