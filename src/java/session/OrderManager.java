/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import cart.ShoppingCart;
import cart.ShoppingCartItem;
import entity.Clients;
import entity.Commandes;
import entity.Produits;
import entity.ProduitsCommandesRetouches;
import entity.ProduitsCommandesRetouchesPK;
import entity.Retouches;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author Jean
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class OrderManager {

    @PersistenceContext(unitName = "WeDressYouPU")
    private EntityManager em;

    @EJB
    private EmailSessionBean emailBean ;
    
    @Resource
    private SessionContext context;

    @EJB
    private ProduitsFacade productFacade;
    @EJB
    private CommandesFacade customerOrderFacade;
    @EJB
    private ProduitsCommandesRetouchesFacade orderedProductFacade;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int placeOrder(String firstName, String familyName, String email, String phone, String address, String ccNumber, ShoppingCart cart) {
        try {
            Clients client = addClient(firstName, familyName, email, phone, address, ccNumber);
            Commandes order = addOrder(client, cart);
            Retouches retouches = addRetouches();
            addOrderedItems(order, retouches, cart);
            sendEmail(email);
            return order.getIdCommandes();
        } catch (Exception e) {
            @SuppressWarnings("ThrowableResultIgnored")
            Exception cause = (Exception) e.getCause();
            if (cause instanceof ConstraintViolationException) {
                @SuppressWarnings("ThrowableResultIgnored")
                ConstraintViolationException cve = (ConstraintViolationException) e.getCause();
                for (Iterator<ConstraintViolation<?>> it = cve.getConstraintViolations().iterator(); it.hasNext();) {
                    ConstraintViolation<? extends Object> v = it.next();
                    System.err.println(v);
                    System.err.println("==>>" + v.getMessage());
                }
            }

            context.setRollbackOnly();
            return 0;
        }
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    private Clients addClient(String firstName, String familyName, String email, String phone, String address, String ccNumber) {
        Clients client = new Clients();
        client.setPrenom(firstName);
        client.setNom(familyName);
        client.setEmail(email);
        client.setTelephone(phone);
        client.setAdresse(address);
        client.setCcNumber(ccNumber);

        em.persist(client);
        return client;
    }

    private Commandes addOrder(Clients client, ShoppingCart cart) {
        // set up customer order
        Commandes order = new Commandes();
        order.setClientsidClients(client);
        order.setMontant(BigDecimal.valueOf(cart.getTotal()));
        // create confirmation number
        Random random = new Random();
        int i = random.nextInt(9999999);
        order.setNumeroConfirmation(i);

        em.persist(order);
        return order;
    }

    private void addOrderedItems(Commandes order, Retouches retouches, ShoppingCart cart) {

        em.flush();
        List<ShoppingCartItem> items = cart.getItems();

        // iterate through shopping cart and create OrderedProducts
        for (ShoppingCartItem scItem : items) {

            int productId = scItem.getProduct().getIdProduits();

            // set up primary key object
            ProduitsCommandesRetouchesPK orderedProductPK = new ProduitsCommandesRetouchesPK();
            orderedProductPK.setCommandesidCommandes(order.getIdCommandes());
            orderedProductPK.setProduitsidProduits(productId);

            
            
            orderedProductPK.setRetouchesidRetouches(retouches.getIdRetouches());
            
            short quantity = (short) scItem.getQuantity();
            
            // create ordered item using PK object
            ProduitsCommandesRetouches orderedItem = new ProduitsCommandesRetouches(orderedProductPK, quantity, "red", "X");
                      
            em.persist(orderedItem);
        }
    }

    public Map getOrderDetails(int orderId) {

        Map orderMap = new HashMap();

        // get order
        Commandes order = customerOrderFacade.find(orderId);

        // get customer
        Clients customer = order.getClientsidClients();

        // get all ordered products
        List<ProduitsCommandesRetouches> orderedProducts = orderedProductFacade.findByOrderId(orderId);

        // get product details for ordered items
        List<Produits> products = new ArrayList<Produits>();

        for (ProduitsCommandesRetouches op : orderedProducts) {

            Produits p = (Produits) productFacade.find(op.getProduitsCommandesRetouchesPK().getProduitsidProduits());
            products.add(p);
        }

        // add each item to orderMap
        orderMap.put("orderRecord", order);
        orderMap.put("customer", customer);
        orderMap.put("orderedProducts", orderedProducts);
        orderMap.put("products", products);

        return orderMap;
    }

    private Retouches addRetouches() {
        em.flush();
        Retouches retouch = new Retouches();
        retouch.setType("largeur");
        retouch.setDescription("largeur");
        em.persist(retouch);
        
        return retouch;
    }
    
    private void sendEmail(String email) {
        String subject = "Votre commande a bien été enregistrée";
        String body = "Merci pour votre commande sur WeDressYou !\n" +
"Celle-ci arrivera dans 48h. Nous vous souhaitons d'excellents essayages !\n" +
"L'équipe de WeDressYou";
        emailBean.sendEmail(email, subject, body);    
    }
}
