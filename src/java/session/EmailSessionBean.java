/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import com.sun.mail.iap.Protocol;
import javax.ejb.Stateless;
import java.util.Properties;
import javax.ejb.LocalBean;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;

/**
 *
 * @author carnelune
 */
@Stateless
public class EmailSessionBean {

    private int port = 587;
    private String host = "smtp.mail.yahoo.com";
    private String from = "WeDressYou@yahoo.fr";
    private boolean auth = true;
    private String username = "WeDressYou@yahoo.fr";
    private String password = "wedressyou2015";
//private Protocol protocol = Protocol.TLS;
    private boolean debug = true;

    public void sendEmail(String to, String subject, String body) {
        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.starttls.enable", true);

        Authenticator authenticator = null;
        if (auth) {
            props.put("mail.smtp.auth", true);
            authenticator = new Authenticator() {
                private PasswordAuthentication pa = new PasswordAuthentication(username, password);

                @Override
                public PasswordAuthentication getPasswordAuthentication() {
                    return pa;
                }
            };

            Session session = Session.getInstance(props, authenticator);
            session.setDebug(debug);

            MimeMessage message = new MimeMessage(session);
            try {
                message.setFrom(new InternetAddress(from));
                InternetAddress[] address = {new InternetAddress(to)};
                message.setRecipients(Message.RecipientType.TO, address);
                message.setSubject(subject);
                message.setSentDate(new Date());
                //
                message.setText(body);
                //Multipart multipart = new MimeMultipart("alternative");
//MimeBodyPart textPart = new MimeBodyPart();
//String textContent = "Hi, Nice to meet you!";
//textPart.setText(textContent);
//MimeBodyPart htmlPart = new MimeBodyPart();
//String htmlContent = "<html><h1>Hi</h1><p>Nice to meet you!</p></html>";
//htmlPart.setContent(htmlContent, "text/html");
//multipart.addBodyPart(textPart);
//multipart.addBodyPart(htmlPart);
//message.setContent(multipart);
                Transport.send(message);
            } catch (MessagingException ex) {
                ex.printStackTrace();
            }
        }

    }
}
