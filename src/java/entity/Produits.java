/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jean
 */
@Entity
@Table(name = "produits")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produits.findAll", query = "SELECT p FROM Produits p"),
    @NamedQuery(name = "Produits.findByIdProduits", query = "SELECT p FROM Produits p WHERE p.idProduits = :idProduits"),
    @NamedQuery(name = "Produits.findByNom", query = "SELECT p FROM Produits p WHERE p.nom = :nom"),
    @NamedQuery(name = "Produits.findByPrix", query = "SELECT p FROM Produits p WHERE p.prix = :prix"),
    @NamedQuery(name = "Produits.findByDerniereMaj", query = "SELECT p FROM Produits p WHERE p.derniereMaj = :derniereMaj"),
    @NamedQuery(name = "Produits.findByDisponibilite", query = "SELECT p FROM Produits p WHERE p.disponibilite = :disponibilite")})
public class Produits implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idProduits")
    private Integer idProduits;
    @Basic(optional = false)
    //@NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nom")
    private String nom;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    //@NotNull
    @Column(name = "prix")
    private BigDecimal prix;
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    //@NotNull
    @Column(name = "derniereMaj")
    @Temporal(TemporalType.TIMESTAMP)
    private Date derniereMaj;
    @Basic(optional = false)
    //@NotNull
    @Column(name = "disponibilite")
    private int disponibilite;
    @JoinColumn(name = "Categories_idCategories", referencedColumnName = "idCategories")
    @ManyToOne(optional = false)
    private Categories categoriesidCategories;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "produits")
    private Collection<ProduitsCommandesRetouches> produitsCommandesRetouchesCollection;

    public Produits() {
    }

    public Produits(Integer idProduits) {
        this.idProduits = idProduits;
    }

    public Produits(Integer idProduits, String nom, BigDecimal prix, Date derniereMaj, int disponibilite) {
        this.idProduits = idProduits;
        this.nom = nom;
        this.prix = prix;
        this.derniereMaj = derniereMaj;
        this.disponibilite = disponibilite;
    }

    public Integer getIdProduits() {
        return idProduits;
    }

    public void setIdProduits(Integer idProduits) {
        this.idProduits = idProduits;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public BigDecimal getPrix() {
        return prix;
    }

    public void setPrix(BigDecimal prix) {
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDerniereMaj() {
        return derniereMaj;
    }

    public void setDerniereMaj(Date derniereMaj) {
        this.derniereMaj = derniereMaj;
    }

    public int getDisponibilite() {
        return disponibilite;
    }

    public void setDisponibilite(int disponibilite) {
        this.disponibilite = disponibilite;
    }

    public Categories getCategoriesidCategories() {
        return categoriesidCategories;
    }

    public void setCategoriesidCategories(Categories categoriesidCategories) {
        this.categoriesidCategories = categoriesidCategories;
    }

    @XmlTransient
    public Collection<ProduitsCommandesRetouches> getProduitsCommandesRetouchesCollection() {
        return produitsCommandesRetouchesCollection;
    }

    public void setProduitsCommandesRetouchesCollection(Collection<ProduitsCommandesRetouches> produitsCommandesRetouchesCollection) {
        this.produitsCommandesRetouchesCollection = produitsCommandesRetouchesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProduits != null ? idProduits.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produits)) {
            return false;
        }
        Produits other = (Produits) object;
        if ((this.idProduits == null && other.idProduits != null) || (this.idProduits != null && !this.idProduits.equals(other.idProduits))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Produits[ idProduits=" + idProduits + " ]";
    }
    
}
