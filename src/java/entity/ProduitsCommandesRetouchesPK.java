/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Jean
 */
@Embeddable
public class ProduitsCommandesRetouchesPK implements Serializable {

    @Basic(optional = false)
    //@NotNull
    @Column(name = "Produits_idProduits")
    private int produitsidProduits;
    @Basic(optional = false)
    //@NotNull
    @Column(name = "Commandes_idCommandes")
    private int commandesidCommandes;
    @Basic(optional = false)
    //@NotNull
    @Column(name = "Retouches_idRetouches")
    private int retouchesidRetouches;

    public ProduitsCommandesRetouchesPK() {
    }

    public ProduitsCommandesRetouchesPK(int produitsidProduits, int commandesidCommandes, int retouchesidRetouches) {
        this.produitsidProduits = produitsidProduits;
        this.commandesidCommandes = commandesidCommandes;
        this.retouchesidRetouches = retouchesidRetouches;
    }

    public int getProduitsidProduits() {
        return produitsidProduits;
    }

    public void setProduitsidProduits(int produitsidProduits) {
        this.produitsidProduits = produitsidProduits;
    }

    public int getCommandesidCommandes() {
        return commandesidCommandes;
    }

    public void setCommandesidCommandes(int commandesidCommandes) {
        this.commandesidCommandes = commandesidCommandes;
    }

    public int getRetouchesidRetouches() {
        return retouchesidRetouches;
    }

    public void setRetouchesidRetouches(int retouchesidRetouches) {
        this.retouchesidRetouches = retouchesidRetouches;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) produitsidProduits;
        hash += (int) commandesidCommandes;
        hash += (int) retouchesidRetouches;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProduitsCommandesRetouchesPK)) {
            return false;
        }
        ProduitsCommandesRetouchesPK other = (ProduitsCommandesRetouchesPK) object;
        if (this.produitsidProduits != other.produitsidProduits) {
            return false;
        }
        if (this.commandesidCommandes != other.commandesidCommandes) {
            return false;
        }
        if (this.retouchesidRetouches != other.retouchesidRetouches) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProduitsCommandesRetouchesPK[ produitsidProduits=" + produitsidProduits + ", commandesidCommandes=" + commandesidCommandes + ", retouchesidRetouches=" + retouchesidRetouches + " ]";
    }
    
}
