/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jean
 */
@Entity
@Table(name = "commandes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Commandes.findAll", query = "SELECT c FROM Commandes c"),
    @NamedQuery(name = "Commandes.findByIdCommandes", query = "SELECT c FROM Commandes c WHERE c.idCommandes = :idCommandes"),
    @NamedQuery(name = "Commandes.findByMontant", query = "SELECT c FROM Commandes c WHERE c.montant = :montant"),
    @NamedQuery(name = "Commandes.findByDateCreation", query = "SELECT c FROM Commandes c WHERE c.dateCreation = :dateCreation"),
    @NamedQuery(name = "Commandes.findByNumeroConfirmation", query = "SELECT c FROM Commandes c WHERE c.numeroConfirmation = :numeroConfirmation")})
public class Commandes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCommandes")
    private Integer idCommandes;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    
    @Column(name = "montant")
    private BigDecimal montant;
    @Basic(optional = false)
    
    @Column(name = "dateCreation")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Basic(optional = false)
    
    @Column(name = "numeroConfirmation")
    private int numeroConfirmation;
    @JoinColumn(name = "Clients_idClients", referencedColumnName = "idClients")
    @ManyToOne(optional = false)
    private Clients clientsidClients;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "commandes")
    private Collection<ProduitsCommandesRetouches> produitsCommandesRetouchesCollection;

    public Commandes() {
    }

    public Commandes(Integer idCommandes) {
        this.idCommandes = idCommandes;
    }

    public Commandes(Integer idCommandes, BigDecimal montant, Date dateCreation, int numeroConfirmation) {
        this.idCommandes = idCommandes;
        this.montant = montant;
        this.dateCreation = dateCreation;
        this.numeroConfirmation = numeroConfirmation;
    }

    public Integer getIdCommandes() {
        return idCommandes;
    }

    public void setIdCommandes(Integer idCommandes) {
        this.idCommandes = idCommandes;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public int getNumeroConfirmation() {
        return numeroConfirmation;
    }

    public void setNumeroConfirmation(int numeroConfirmation) {
        this.numeroConfirmation = numeroConfirmation;
    }

    public Clients getClientsidClients() {
        return clientsidClients;
    }

    public void setClientsidClients(Clients clientsidClients) {
        this.clientsidClients = clientsidClients;
    }

    @XmlTransient
    public Collection<ProduitsCommandesRetouches> getProduitsCommandesRetouchesCollection() {
        return produitsCommandesRetouchesCollection;
    }

    public void setProduitsCommandesRetouchesCollection(Collection<ProduitsCommandesRetouches> produitsCommandesRetouchesCollection) {
        this.produitsCommandesRetouchesCollection = produitsCommandesRetouchesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCommandes != null ? idCommandes.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commandes)) {
            return false;
        }
        Commandes other = (Commandes) object;
        if ((this.idCommandes == null && other.idCommandes != null) || (this.idCommandes != null && !this.idCommandes.equals(other.idCommandes))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Commandes[ idCommandes=" + idCommandes + " ]";
    }
    
}
