/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jean
 */
@Entity
@Table(name = "profils")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Profils.findAll", query = "SELECT p FROM Profils p"),
    @NamedQuery(name = "Profils.findByIdProfils", query = "SELECT p FROM Profils p WHERE p.idProfils = :idProfils"),
    @NamedQuery(name = "Profils.findBySexe", query = "SELECT p FROM Profils p WHERE p.sexe = :sexe"),
    @NamedQuery(name = "Profils.findByCouleur", query = "SELECT p FROM Profils p WHERE p.couleur = :couleur"),
    @NamedQuery(name = "Profils.findByTaille", query = "SELECT p FROM Profils p WHERE p.taille = :taille")})
public class Profils implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idProfils")
    private Integer idProfils;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "sexe")
    private String sexe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "couleur")
    private String couleur;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "taille")
    private String taille;
    @JoinColumn(name = "Clients_idClients", referencedColumnName = "idClients")
    @ManyToOne(optional = false)
    private Clients clientsidClients;

    public Profils() {
    }

    public Profils(Integer idProfils) {
        this.idProfils = idProfils;
    }

    public Profils(Integer idProfils, String sexe, String couleur, String taille) {
        this.idProfils = idProfils;
        this.sexe = sexe;
        this.couleur = couleur;
        this.taille = taille;
    }

    public Integer getIdProfils() {
        return idProfils;
    }

    public void setIdProfils(Integer idProfils) {
        this.idProfils = idProfils;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public String getTaille() {
        return taille;
    }

    public void setTaille(String taille) {
        this.taille = taille;
    }

    public Clients getClientsidClients() {
        return clientsidClients;
    }

    public void setClientsidClients(Clients clientsidClients) {
        this.clientsidClients = clientsidClients;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProfils != null ? idProfils.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Profils)) {
            return false;
        }
        Profils other = (Profils) object;
        if ((this.idProfils == null && other.idProfils != null) || (this.idProfils != null && !this.idProfils.equals(other.idProfils))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Profils[ idProfils=" + idProfils + " ]";
    }
    
}
