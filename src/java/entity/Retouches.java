/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jean
 */
@Entity
@Table(name = "retouches")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Retouches.findAll", query = "SELECT r FROM Retouches r"),
    @NamedQuery(name = "Retouches.findByIdRetouches", query = "SELECT r FROM Retouches r WHERE r.idRetouches = :idRetouches"),
    @NamedQuery(name = "Retouches.findByType", query = "SELECT r FROM Retouches r WHERE r.type = :type"),
    @NamedQuery(name = "Retouches.findByDescription", query = "SELECT r FROM Retouches r WHERE r.description = :description")})
public class Retouches implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idRetouches")
    private Integer idRetouches;
    @Basic(optional = false)
    //@NotNull
    @Size(min = 1, max = 8)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    //@NotNull
    @Size(min = 1, max = 255)
    @Column(name = "description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "retouches")
    private Collection<ProduitsCommandesRetouches> produitsCommandesRetouchesCollection;

    public Retouches() {
    }

    public Retouches(Integer idRetouches) {
        this.idRetouches = idRetouches;
    }

    public Retouches(Integer idRetouches, String type, String description) {
        this.idRetouches = idRetouches;
        this.type = type;
        this.description = description;
    }

    public Integer getIdRetouches() {
        return idRetouches;
    }

    public void setIdRetouches(Integer idRetouches) {
        this.idRetouches = idRetouches;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<ProduitsCommandesRetouches> getProduitsCommandesRetouchesCollection() {
        return produitsCommandesRetouchesCollection;
    }

    public void setProduitsCommandesRetouchesCollection(Collection<ProduitsCommandesRetouches> produitsCommandesRetouchesCollection) {
        this.produitsCommandesRetouchesCollection = produitsCommandesRetouchesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRetouches != null ? idRetouches.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Retouches)) {
            return false;
        }
        Retouches other = (Retouches) object;
        if ((this.idRetouches == null && other.idRetouches != null) || (this.idRetouches != null && !this.idRetouches.equals(other.idRetouches))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Retouches[ idRetouches=" + idRetouches + " ]";
    }
    
}
