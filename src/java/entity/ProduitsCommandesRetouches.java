/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jean
 */
@Entity
@Table(name = "produits_commandes_retouches")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProduitsCommandesRetouches.findAll", query = "SELECT p FROM ProduitsCommandesRetouches p"),
    @NamedQuery(name = "ProduitsCommandesRetouches.findByProduitsidProduits", query = "SELECT p FROM ProduitsCommandesRetouches p WHERE p.produitsCommandesRetouchesPK.produitsidProduits = :produitsidProduits"),
    @NamedQuery(name = "ProduitsCommandesRetouches.findByCommandesidCommandes", query = "SELECT p FROM ProduitsCommandesRetouches p WHERE p.produitsCommandesRetouchesPK.commandesidCommandes = :commandesidCommandes"),
    @NamedQuery(name = "ProduitsCommandesRetouches.findByRetouchesidRetouches", query = "SELECT p FROM ProduitsCommandesRetouches p WHERE p.produitsCommandesRetouchesPK.retouchesidRetouches = :retouchesidRetouches"),
    @NamedQuery(name = "ProduitsCommandesRetouches.findByQuantite", query = "SELECT p FROM ProduitsCommandesRetouches p WHERE p.quantite = :quantite"),
    @NamedQuery(name = "ProduitsCommandesRetouches.findByCouleur", query = "SELECT p FROM ProduitsCommandesRetouches p WHERE p.couleur = :couleur"),
    @NamedQuery(name = "ProduitsCommandesRetouches.findByTaille", query = "SELECT p FROM ProduitsCommandesRetouches p WHERE p.taille = :taille")})
public class ProduitsCommandesRetouches implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProduitsCommandesRetouchesPK produitsCommandesRetouchesPK;
    @Basic(optional = false)
    //@NotNull
    @Column(name = "quantite")
    private short quantite;
    @Basic(optional = false)
    //@NotNull
    @Size(min = 1, max = 45)
    @Column(name = "couleur")
    private String couleur;
    @Basic(optional = false)
    //@NotNull
    @Size(min = 1, max = 3)
    @Column(name = "taille")
    private String taille;
    @JoinColumn(name = "Commandes_idCommandes", referencedColumnName = "idCommandes", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Commandes commandes;
    @JoinColumn(name = "Produits_idProduits", referencedColumnName = "idProduits", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Produits produits;
    @JoinColumn(name = "Retouches_idRetouches", referencedColumnName = "idRetouches", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Retouches retouches;

    public ProduitsCommandesRetouches() {
    }

    public ProduitsCommandesRetouches(ProduitsCommandesRetouchesPK produitsCommandesRetouchesPK) {
        this.produitsCommandesRetouchesPK = produitsCommandesRetouchesPK;
    }

    public ProduitsCommandesRetouches(ProduitsCommandesRetouchesPK produitsCommandesRetouchesPK, short quantite, String couleur, String taille) {
        this.produitsCommandesRetouchesPK = produitsCommandesRetouchesPK;
        this.quantite = quantite;
        this.couleur = couleur;
        this.taille = taille;
    }

    public ProduitsCommandesRetouches(int produitsidProduits, int commandesidCommandes, int retouchesidRetouches) {
        this.produitsCommandesRetouchesPK = new ProduitsCommandesRetouchesPK(produitsidProduits, commandesidCommandes, retouchesidRetouches);
    }

    public ProduitsCommandesRetouchesPK getProduitsCommandesRetouchesPK() {
        return produitsCommandesRetouchesPK;
    }

    public void setProduitsCommandesRetouchesPK(ProduitsCommandesRetouchesPK produitsCommandesRetouchesPK) {
        this.produitsCommandesRetouchesPK = produitsCommandesRetouchesPK;
    }

    public short getQuantite() {
        return quantite;
    }

    public void setQuantite(short quantite) {
        this.quantite = quantite;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public String getTaille() {
        return taille;
    }

    public void setTaille(String taille) {
        this.taille = taille;
    }

    public Commandes getCommandes() {
        return commandes;
    }

    public void setCommandes(Commandes commandes) {
        this.commandes = commandes;
    }

    public Produits getProduits() {
        return produits;
    }

    public void setProduits(Produits produits) {
        this.produits = produits;
    }

    public Retouches getRetouches() {
        return retouches;
    }

    public void setRetouches(Retouches retouches) {
        this.retouches = retouches;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (produitsCommandesRetouchesPK != null ? produitsCommandesRetouchesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProduitsCommandesRetouches)) {
            return false;
        }
        ProduitsCommandesRetouches other = (ProduitsCommandesRetouches) object;
        if ((this.produitsCommandesRetouchesPK == null && other.produitsCommandesRetouchesPK != null) || (this.produitsCommandesRetouchesPK != null && !this.produitsCommandesRetouchesPK.equals(other.produitsCommandesRetouchesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ProduitsCommandesRetouches[ produitsCommandesRetouchesPK=" + produitsCommandesRetouchesPK + " ]";
    }
    
}
