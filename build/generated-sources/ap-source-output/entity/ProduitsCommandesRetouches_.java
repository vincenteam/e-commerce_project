package entity;

import entity.Commandes;
import entity.Produits;
import entity.ProduitsCommandesRetouchesPK;
import entity.Retouches;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-12-15T14:36:20")
@StaticMetamodel(ProduitsCommandesRetouches.class)
public class ProduitsCommandesRetouches_ { 

    public static volatile SingularAttribute<ProduitsCommandesRetouches, Produits> produits;
    public static volatile SingularAttribute<ProduitsCommandesRetouches, Commandes> commandes;
    public static volatile SingularAttribute<ProduitsCommandesRetouches, String> taille;
    public static volatile SingularAttribute<ProduitsCommandesRetouches, Retouches> retouches;
    public static volatile SingularAttribute<ProduitsCommandesRetouches, ProduitsCommandesRetouchesPK> produitsCommandesRetouchesPK;
    public static volatile SingularAttribute<ProduitsCommandesRetouches, String> couleur;
    public static volatile SingularAttribute<ProduitsCommandesRetouches, Short> quantite;

}