package entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-12-15T14:36:20")
@StaticMetamodel(Clients.class)
public class Clients_ { 

    public static volatile SingularAttribute<Clients, String> ccNumber;
    public static volatile SingularAttribute<Clients, Integer> idClients;
    public static volatile SingularAttribute<Clients, String> adresse;
    public static volatile SingularAttribute<Clients, String> telephone;
    public static volatile SingularAttribute<Clients, String> nom;
    public static volatile SingularAttribute<Clients, String> prenom;
    public static volatile SingularAttribute<Clients, String> email;

}