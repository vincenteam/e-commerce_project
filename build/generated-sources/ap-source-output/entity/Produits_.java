package entity;

import entity.Categories;
import entity.ProduitsCommandesRetouches;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-12-15T14:36:20")
@StaticMetamodel(Produits.class)
public class Produits_ { 

    public static volatile SingularAttribute<Produits, Integer> idProduits;
    public static volatile SingularAttribute<Produits, BigDecimal> prix;
    public static volatile SingularAttribute<Produits, Integer> disponibilite;
    public static volatile CollectionAttribute<Produits, ProduitsCommandesRetouches> produitsCommandesRetouchesCollection;
    public static volatile SingularAttribute<Produits, String> description;
    public static volatile SingularAttribute<Produits, String> nom;
    public static volatile SingularAttribute<Produits, Date> derniereMaj;
    public static volatile SingularAttribute<Produits, Categories> categoriesidCategories;

}