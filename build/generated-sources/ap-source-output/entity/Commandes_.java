package entity;

import entity.Clients;
import entity.ProduitsCommandesRetouches;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-12-15T14:36:20")
@StaticMetamodel(Commandes.class)
public class Commandes_ { 

    public static volatile SingularAttribute<Commandes, Date> dateCreation;
    public static volatile SingularAttribute<Commandes, Integer> idCommandes;
    public static volatile SingularAttribute<Commandes, Clients> clientsidClients;
    public static volatile CollectionAttribute<Commandes, ProduitsCommandesRetouches> produitsCommandesRetouchesCollection;
    public static volatile SingularAttribute<Commandes, BigDecimal> montant;
    public static volatile SingularAttribute<Commandes, Integer> numeroConfirmation;

}