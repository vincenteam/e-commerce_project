package entity;

import entity.ProduitsCommandesRetouches;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-12-15T14:36:20")
@StaticMetamodel(Retouches.class)
public class Retouches_ { 

    public static volatile SingularAttribute<Retouches, Integer> idRetouches;
    public static volatile CollectionAttribute<Retouches, ProduitsCommandesRetouches> produitsCommandesRetouchesCollection;
    public static volatile SingularAttribute<Retouches, String> description;
    public static volatile SingularAttribute<Retouches, String> type;

}