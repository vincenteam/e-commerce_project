package entity;

import entity.Produits;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-12-15T14:36:20")
@StaticMetamodel(Categories.class)
public class Categories_ { 

    public static volatile CollectionAttribute<Categories, Produits> produitsCollection;
    public static volatile SingularAttribute<Categories, Integer> idCategories;
    public static volatile SingularAttribute<Categories, String> nom;

}