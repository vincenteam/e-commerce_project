<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%-- 
    Document   : index
    Created on : Dec 1, 2015, 9:44:37 AM
    Author     : Jean
--%>
<%--<sql:query var="categories" dataSource="jdbc/mydb">
    SELECT * FROM categories
</sql:query>--%>
<!-- Page Content -->
<div class="container">
    <div class="col-md-12">
        <div class="row">
            <c:forEach var="category" items="${categories}">
                <div class="col-sm-4 col-lg-4 col-md-4">
                    <div class="thumbnail">
                        <a href="category?${category.idCategories}">
                            <img src="${initParam.categoryImagePath}${category.nom}.jpg"
                                 class="img-responsive"
                                 alt="<fmt:message key='${category.nom}'/>">
                            <h4 class="caption"><fmt:message key='${category.nom}'/></h4>
                        </a>                                             
                    </div>
                </div>
            </c:forEach>                
        </div>
    </div>
</div>


