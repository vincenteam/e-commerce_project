<%-- 
    Document   : cart
    Created on : Dec 1, 2015, 9:57:15 PM
    Author     : Jean
--%>

<!-- Page Content -->
<div class="container">

    <div class="row">
        <c:set var="value">
            <c:choose>
                <%-- if 'selectedCategory' session object exists, send user to previously viewed category --%>
                <c:when test="${!empty selectedCategory}">
                    category
                </c:when>
                <%-- otherwise send user to welcome page --%>
                <c:otherwise>
                    index.jsp
                </c:otherwise>
            </c:choose>
        </c:set>

        <div class="col-md-12">
            <div class="panel panel-title">                            
                <div class="panel-heading panel-success"><fmt:message key='Cart'/>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <a href="${value}" class="btn btn-info"><span class="glyphicons glyphicons-left-arrow"></span><fmt:message key='Continue Shopping'/></a>
                </div>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th style="width:10%;"><fmt:message key='Products'/></th>
                        <th style="width:30%;"><fmt:message key='Name'/></th>
                        <th style="width:30%;"><fmt:message key='Price'/></th>
                        <th style="width:30%;"><fmt:message key='Quantity'/></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="cartItem" items="${cart.items}" varStatus="iter">

                        <c:set var="product" value="${cartItem.product}"/>

                        <tr>
                            <td>
                                <img src="${initParam.productImagePath}${product.nom}.jpg"
                                     class="img-responsive"
                                     alt="${product.nom}">
                            </td>

                            <td>${product.nom}</td>

                            <td>
                                &euro; ${cartItem.total}
                                <br>
                                <span class="smallText">( &euro; ${product.prix} / unit )</span>
                            </td>

                            <td>
                                <form action="updateCart" method="post">
                                    <input type="hidden"
                                           name="productId"
                                           value="${product.idProduits}">
                                    <input type="text"
                                           maxlength="2"
                                           size="2"
                                           value="${cartItem.quantity}"
                                           name="quantity"
                                           style="margin:5px">
                                    <input type="submit"
                                           name="submit"
                                           value="<fmt:message key='Update'/>">
                                </form>
                            </td>
                        </tr>
                    </c:forEach>

                </tbody>
            </table>
            <c:choose>
                <c:when test="${!empty cart && cart.numberOfItems != 0}">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="pull-left">
                                <a href="viewCart?clear=true" class="btn"><fmt:message key='Clear cart'/></a>
                            </div>

                            <div class="pull-right">
                                <a href="checkout" class="btn btn-primary"><fmt:message key='Proceed to checkout'/></a> 
                            </div>

                        </div>
                    </div>
                </c:when>
            </c:choose>

            <div class="panel panel-default">
                <div class="panel-footer clearfix pull-right">                  
                    <h4 id="subtotal"><fmt:message key='Subtotal'/>: &euro; ${cart.subtotal}</h4>
                </div>

            </div>
        </div>          
    </div>

</div>

<!-- /.container -->
