<%-- 
    Document   : confirmation
    Created on : Dec 1, 2015, 10:02:29 PM
    Author     : Jean
--%>

<div class="container">
    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title"><fmt:message key='Congratulation'/>!</h3>
            </div>
            <div class="panel-body"><fmt:message key='ConfirmationText'/></div>
        </div>
        
        <div class="col-md-6" >

            <table id="orderSummaryTable" class="table table-striped">
                
                <p><fmt:message key='Summary'/>:</p>
                
                <thead>
                <tr>
                    <th style="width:33%;"><fmt:message key='Products'/></th>
                    <th style="width:33%;"><fmt:message key='Quantity'/></th>
                    <th style="width:34%;"><fmt:message key='Price'/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="orderedProduct" items="${orderedProducts}" varStatus="iter">

                    <tr>
                        <td>${products[iter.index].nom}</td>
                        <td class="quantityColumn">
                            ${orderedProduct.quantite}
                        </td>
                        <td class="confirmationPriceColumn">
                            &euro; ${products[iter.index].prix * orderedProduct.quantite}
                        </td>
                    </tr>

                </c:forEach>

                

                <tr>
                    <td colspan="2" id="deliverySurchargeCellLeft"><fmt:message key='DeliverySurcharge'/>:</td>
                    <td id="deliverySurchargeCellRight">&euro; ${initParam.deliverySurcharge}</td>
                </tr>

                <tr>
                    <td colspan="2" id="totalCellLeft"><fmt:message key='Total'/>:</td>
                    <td id="totalCellRight">&euro; ${orderRecord.montant}</td>
                </tr>

                

                <tr>
                    <td colspan="3" id="dateProcessedRow"><fmt:message key='DateProcessed'/>:
                        ${orderRecord.dateCreation}
                    </td>
                </tr>
                </tbody>
            </table>

        </div>

        <div class="col-md-6" >

            <table id="deliveryAddressTable" class="table">
                <p><fmt:message key='DeliveryAddress'/></p>
                    
                <tbody>
                    <tr>
                        <td colspan="3" class="lightBlue">
                            ${customer.prenom}
                            <br>
                            ${customer.nom}
                            <br>
                            ${customer.adresse}
                            <br>
                                                      
                            <hr>
                            <fmt:message key='Email'/>: ${customer.email}
                            <br>
                            <fmt:message key='Phone'/>: ${customer.telephone}
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
</div>